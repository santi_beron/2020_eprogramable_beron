/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/

//#define ON=1;
//#define OFF=2;
//#define TOGGLE=3;

/*

A) Realice un función  que reciba un puntero a una estructura LED como la que se muestra a
continuación:

struct leds
{
	uint8_t n_led;        indica el número de led a controlar
	uint8_t n_ciclos;   indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    indica el tiempo de cada ciclo
	uint8_t mode;       ON, OFF, TOGGLE
} my_leds;

 */
typedef enum
{
	ON=1,
	OFF,
	TOGGLE,
}state_t;


typedef enum
{
	led_1=1,
	led_2,
	led_3,

}led_t;


typedef struct
{
	uint8_t n_led;       //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;       //ON, OFF, TOGGLE
} leds;

uint8_t i;
uint8_t j;

void LedControl(leds *l);


/*==================[internal functions declaration]=========================*/

void LedControl(leds *l)
{

	switch(l->mode)
	{
	case ON:
		switch(l->n_led)
		{

		case led_1:
			printf("\nse enciende %d" , led_1);
			break;

		case led_2:
			printf("\nse enciende %d" , led_2);
			break;

		case led_3:
			printf("\nse enciende %d" , led_3);
			break;
		}

		break;

	case OFF:

		switch(l->n_led)
		{

		case led_1:
			printf("\nse apaga %d" , led_1);
			break;

		case led_2:
			printf("\nse apaga %d" , led_2);
			break;

		case led_3:
			printf("\nse apaga %d" , led_3);
			break;

		break;
		}

	case TOGGLE:

		for(i=0;i<l->n_ciclos;i++)
		{
			switch(l->n_led)
				{
					case led_1:
						printf("\nse togglea %d" , led_1);
						break;

					case led_2:
						printf("\nse togglea %d" , led_2);
						break;

					case led_3:
						printf("\nse togglea %d" , led_3);
						break;

				}

			for(j=0;j<l->periodo;j++){};
		}

		break;


		}

}

int main(void)
{
    leds mi_led={led_1, 10, 1, TOGGLE };
    LedControl(&mi_led);

    return 0;
}

/*==================[end of file]============================================*/

