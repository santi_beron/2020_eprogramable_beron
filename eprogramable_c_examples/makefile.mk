########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = Guia1_EjerciciosVarios
#NOMBRE_EJECUTABLE = Guia1_EjerciciosVarios.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio14
#NOMBRE_EJECUTABLE = Guia1_Ejercicio14.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio16
#NOMBRE_EJECUTABLE = Guia1_Ejercicio16.exe

#PROYECTO_ACTIVO = Ejercicio16
#NOMBRE_EJECUTABLE = Ejercicio16.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio_17_18
#NOMBRE_EJECUTABLE = Guia1_Ejercicio_17_18.exe

PROYECTO_ACTIVO = Guia1_EjercicioIntegrador_A
NOMBRE_EJECUTABLE = Guia1_EjercicioIntegrador_A.exe

#PROYECTO_ACTIVO = Guia1_EjercicioIntegrador_C_D
#NOMBRE_EJECUTABLE = Guia1_EjercicioIntegrador_C_D.exe