/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>


/*==================[macros and definitions]=================================*/

#define CANTIDAD_PINES	4


#define PIN1	1
#define PUERTO1	1
#define SALIDA	1



/*
C) Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida y
un puntero a un arreglo donde se almacene los n dígitos. La función deberá convertir el
dato recibido a BCD, guardando cada uno de los dígitos de salida en el arreglo pasado como
puntero.

*/

/*

D) Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo  gpioConf_t.

typedef struct
{
	uint8_t port;				/*!< GPIO port number
    uint8_t pin;				/*!< GPIO pin number
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT /
} gpioConf_t;

Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14

*/

void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{

	uint8_t i=0;

	for(i=0;i<digits;i++)
	//while(digits>0)
	{
		//digits--;
		//printf("\ndigito: %d :", digits);
		printf("\ndigito: %d", i);
		//bcd_number[digits]=data%10;
		bcd_number[i]=data%10;
		//printf("\resto: %d :", bcd_number[digits]);
		printf("\nresto: %d", bcd_number[i]);
		data=data/10;

	}

}


typedef struct
	{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
	} gpioConf_t;


void BcdToLcd(uint8_t digito, gpioConf_t * configuracion  ) //0b0000
{

	uint8_t i;

	/* b0*/
	for(i = 0 ; i < CANTIDAD_PINES; i++)
	{
		//equivale a d

	if(digito & 1)
	{
		printf("\nSe pone a 1 el pin %d del puerto %d", configuracion[i].pin, configuracion[i].port);

	}

	else
	{
		printf("\nSe pone a 0 el pin %d del puerto %d", configuracion[i].pin, configuracion[i].port);

	}

	digito=digito>>1;
	}



}

/*==================[internal functions declaration]=========================*/

int main(void)
{
	gpioConf_t conf[]={{1,4,1},{1,5,1},{1,6,1},{2,14,1}};
	uint32_t dato=1234;
	uint8_t numeros[4];
	BinaryToBcd(dato, 4, numeros);

	printf("\ndigito: %d \r\n", numeros[2]);
	BcdToLcd(8, conf); //0n1000

	return 0;
}

/*==================[end of file]============================================*/

