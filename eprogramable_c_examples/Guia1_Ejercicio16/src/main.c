/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>


/*==================[macros and definitions]=================================*/
/*
 *
 * Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 * Declare cuatro variables sin signo de 8 bits y, utilizando máscaras,
 * rotaciones y truncamiento, cargue cada uno de los bytes de la variable de
 * 32 bits.
 * Realice el mismo ejercicio, utilizando la definición de una “union”.
 *
 */

#define numBITS=8;

union Union32_8{
struct cada_byte
	{
    uint8_t byte1;
 	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
	}byte;
uint32_t  todos_los_bytes;
}num;


/*==================[internal functions declaration]=========================*/

int main(void)
{
	uint32_t variable= 0x01020304;
	//mascara con 8 bits en 1
	uint32_t mascara=0x000000FF;
	printf("\Mascara : %d \r\n", mascara);

	uint8_t p1, p2, p3, p4;

	//uint16_t a=0x123;
	//uint8_t b;
	//uint8_t c;
	//b=(uint8_t) a  //tomo los 8 bits menos significativos
	//c=(uint8_t) a>>8  //tomo los 8 bits más significativos


	p1= variable & mascara;
	p2= (variable>>8) & mascara;
	p3= (variable>>16) & mascara;
	p4= (variable>>24) & mascara;

	printf("\nVariable : %d", variable);
	printf("\nparte 1 : %d", p1);
	printf("\nparte 2 : %d", p2);
	printf("\nparte 3 : %d", p3);
	printf("\nparte 4 : %d", p4);

//--------------------------------------------------------------------



	num.todos_los_bytes=0x01020304;
	printf("\nVariable: %d \r\n", num.todos_los_bytes);
	printf("\Parte 1 : %d \r\n", num.byte.byte1);
	printf("\Parte 2 : %d \r\n", num.byte.byte2);
	printf("\Parte 3 : %d \r\n", num.byte.byte3);
	printf("\Parte 4 : %d \r\n", num.byte.byte4);







	return 0;
}

/*==================[end of file]============================================*/

