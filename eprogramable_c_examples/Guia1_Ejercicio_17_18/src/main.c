/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
/* 17
 * Realice un programa que calcule el promedio de los 15 números listados abajo, para ello,
 * rimero realice un diagrama de flujo similar al presentado en el ejercicio 9.
 * (Puede utilizar la aplicación Draw.io). Para la implementación, utilice el menor tamaño de
 * datos posible...
 *
 * 18
 * Al ejercicio anterior agregue un número más (16 en total), modifique su programa de manera
 * que agregue el número extra a la suma e intente no utilizar el operando división “/”
 * N°16 =>233. Si utiliza las directivas de preprocesador ( #ifdef, #ifndef,#endif, etc) no
 * N°16 necesita generar un nuevo programa
 *
 */




//#define CANTIDAD 15
#define CANT 16
#define CORRIMIENTOS 4


//uint8_t vector[ ]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};
uint8_t vector[ ]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9,233};


/*==================[internal functions declaration]=========================*/

int main(void)
{
    uint16_t sumador=0;
    uint8_t iterador, promedio=0;

    for(iterador=0; iterador<CANT;iterador++)
    {
    	sumador+=vector[iterador];
    }

    printf("Suma de los numeros: %d \n", sumador);
    //prom=(uint8_t)(sumador/CANTIDAD)
    promedio=(uint8_t)(sumador>>CORRIMIENTOS);
    printf("PROMEDIO: %d \n", promedio);



	return 0;
}

/*==================[end of file]============================================*/

