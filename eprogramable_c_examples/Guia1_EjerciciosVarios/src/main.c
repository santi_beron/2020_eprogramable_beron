/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>


/*==================[macros and definitions]=================================*/


#define BIT0 0
#define BIT1 1
#define BIT6 6
#define BIT2 2
#define BIT3 3
#define BIT4 4
#define BIT5 5
#define BIT13 13
#define BIT14 14





/*==================[internal functions declaration]=========================*/

int main(void)
{

/* Declare una constante de 32 bits, con todos los bits en 0 y el bit 6 en 1.
 * Utilice el operador <<
 */

	printf(" \nEjercicio 1 ");

	const uint32_t variable1= 1 << BIT6;
	printf(" \nVariable : %d \r\n " , variable1);

	printf("------------------------------- ");

/* Declare una constante de 16 bits, con todos los bits en 0 y los bits 3 y 4 en 1.
 * Utilice el operador <<.
 */

	printf(" \nEjercicio 2 ");

	const uint16_t variable2=1<<BIT3 | 1<<BIT4;
	printf("\nEl valor de la variable es: %d \r\n",variable2);

	printf("------------------------------- ");


/* Declare una variable de 16 bits sin signo, con el valor inicial 0xFFFF
 * y luego, mediante una operación de máscaras, coloque a 0 el bit 14.
 */

	printf(" \nEjercicio 3 ");

	uint16_t mascara3= 1<<BIT14;
	uint16_t variable3=0xFFFF;
	variable3=variable3 & ~mascara3;
	printf("\nEl valor de la variable es: %d \r\n",variable3);

	printf("------------------------------- ");

/* Declare una variable de 32 bits sin signo, con el valor inicial 0x0000 y luego,
 * mediante una operación de máscaras, coloque a 1 el bit 2.
 */

	printf(" \nEjercicio 4 ");

	uint32_t variable4=0x0000;
	uint32_t mascara4=1<<BIT2;
	variable4= variable4 | mascara4;
	printf("\nEl valor de la variable es: %d \r\n",variable4);

	printf("------------------------------- ");



/* Declare una variable de 32 bits sin signo, con el valor inicial 0x00001234 y luego,
 * mediante una operación de máscaras, invierta el bit 0.
 */

	printf(" \nEjercicio 5 ");

	uint32_t variable5=0x00001234;
	printf("\nVer bit 0 : %d ", variable5);
	uint32_t mascara5=1<<BIT0;
	uint32_t resultado5=variable5 & mascara5;

	if(resultado5==0)
	{
		variable5= variable5 | mascara5;
	}

	else
	{
		mascara5=~mascara5;
		variable5= variable5 & mascara5;
	}

	printf("\nBit 0 invertido: %d \r\n ", variable5);

	printf("------------------------------- ");


/* Sobre una variable de 32 bits sin signo previamente declarada y de valor
 * desconocido, asegúrese de colocar el bit 4 a 0 mediante máscaras y el operador <<.
 */

	printf(" \nEjercicio 6 ");

	uint32_t variable6;
	printf("\nValor inicial de la variable: %d \r\n", variable6);
	uint32_t mascara6=1<<BIT4;
	mascara6=~mascara6;
	variable6=variable6&mascara6;
	printf("Valor final de la variable: %d \r\n", variable6);

	printf("------------------------------- ");


/*Sobre una variable de 32 bits sin signo previamente declarada y de valor desconocido,
 *asegúrese de colocar el bit 3 a 1 y los bits 13 y 14 a 0 mediante máscaras y el operador <<.
 */

	printf(" \nEjercicio 7 ");

	uint32_t variable7;
	printf("El valor de la variable es: %d \r\n", variable7);
	uint32_t mascara7=1<<BIT13 | 1<<BIT14;
	mascara7=~mascara7;
	uint32_t mascara7_1=1<<BIT3;
	uint32_t resultado7=(variable7 & mascara7) | mascara7_1;
	printf("El valor final es: %d \r\n",resultado7);

	printf("------------------------------- ");


/*
* Sobre una variable de 16 bits previamente declarada y de valor desconocido,
* invierta el estado de los bits 3 y 5 mediante máscaras y el operador <<.
*/

	printf(" \nEjercicio 8 ");

	int16_t variable8;

	printf("\nEl valor inicial de la variable es: %d \r\n", variable8);

	int16_t mascara8_1=1<<BIT3;
	int16_t mascara8_2=1<<BIT5;

	if ( ((variable8&mascara8_1)>>BIT3)==0)
	{
	variable8=variable8|mascara8_1;
	}

		else if ( ((variable8&mascara8_1)>>BIT3)==1)
		{
		mascara8_1=~mascara8_1;
		variable8=variable8 & mascara8_1;
		}

	if ( ((variable8&mascara8_2)>>BIT5)==0)
		{
		variable8=variable8 | mascara8_2;
		}

		else if ( ((variable8&mascara8_2)>>BIT5)==1)
		{
		mascara8_2=~mascara8_2;
		variable8=variable8 & mascara8_2;
		}

	printf("El valor final de la variable es: %d \r\n", variable8);

	printf("------------------------------- ");


/* Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0.
 * Si es 0, cargue una variable “A” previamente declarada en 0, si es 1,
 * cargue “A” con 0xaa. Para la resolución de la lógica, siga el diagrama de flujo siguiente...
 */


	printf(" \nEjercicio 9 ");
	const uint32_t constante9;
	const uint32_t mascara9=1<<BIT4;
	uint8_t A;

	if((constante9 & mascara9)==0)
	{
		A=0;
	}
	else
	{
		A=0xaa;
	}

	printf("\nValor de la constante: %d \r\n", constante9);
	printf("A : %d \r\n", A);

	printf("------------------------------- ");

/* Sobre una variable de 16 bits previamente declarada, verifique si el bit 3
 * y el bit 1 son 1.
 */

	printf(" \nEjercicio 10 ");

	int16_t variable10;
	printf("\nEl valor de la variable es : %d \r\n", variable10);

	int16_t mascara10_1=1<<BIT3;
	int16_t mascara10_2=1<BIT1;

	if(((variable10&mascara10_1)>>BIT3) == 1 || ((variable10&mascara10_2)>>BIT1)==1)
		printf("El bit 3 y el bit 1 valen 1");
	else
		printf("El bit 3 y el bit 1 no valen 1 simultaneamente \n ");

	printf("------------------------------- ");

/*  Declare un puntero a un entero con signo de 16 bits y cargue inicialmente el valor -1.
 *  Luego, mediante máscaras, coloque un 0 en el bit 4.
 */

	printf(" \nEjercicio 12 ");

	int16_t ent;
	int16_t *p_ent;
	p_ent=&ent;
	*p_ent=-1;
	//printf("El valor del entero es: %d", *pent);
	uint16_t mascara12 = 1<<BIT4;
	mascara12=~mascara12;
	//printf("El valor de la mascara es: %d", mascara3);
	ent=ent&mascara12;
	printf("\nEl valor del entero es : %d \r\n ", ent);

	printf("------------------------------- ");


/*
 * Cargar la tabla
 */


	printf(" \nEjercicio 13 ");

	printf ("\n");
	printf ("------------------------------------------------------------------------------------");
	printf ("\n");
	printf("  Tipo               Valor maximo              Valor minimo            Capacidad   " );
	printf ("\n");
	printf ("------------------------------------------------------------------------------------");
	printf ("\n");
	printf(" int8_t                   127                  	   -128			   255	   ");
	printf ("\n");
	printf(" int16_t                 32767                    -32768	          65535	   ");
	printf ("\n");
	printf(" int32_t               2147483647               -2147483648	       4294967295	   ");
	printf ("\n");
	printf(" uint8_t                   255                       0	                  256");
	printf ("\n");
	printf(" uint16_t                 65535                      0			 65536   ");
	printf ( "\n");
	printf(" uint32_t              4294967295                    0		       4294967295  ");


//Ejercicio de estructura


	return 0;
}

/*==================[end of file]============================================*/

