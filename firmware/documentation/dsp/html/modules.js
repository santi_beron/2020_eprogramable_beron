var modules =
[
    [ "Basic Math Functions", "group__group_math.html", [
      [ "Introduction", "index.html#autotoc_md0", null ],
      [ "Using the Library", "index.html#autotoc_md1", null ],
      [ "Examples", "index.html#autotoc_md2", null ],
      [ "Toolchain Support", "index.html#autotoc_md3", null ],
      [ "Building the Library", "index.html#autotoc_md4", null ],
      [ "Preprocessor Macros", "index.html#autotoc_md5", null ],
      [ "CMSIS-DSP in ARM::CMSIS Pack", "index.html#autotoc_md6", null ],
      [ "Revision History of CMSIS-DSP", "index.html#autotoc_md7", null ],
      [ "Copyright Notice", "index.html#autotoc_md8", null ]
    ] ],
    [ "Fast Math Functions", "group__group_fast_math.html", "group__group_fast_math" ],
    [ "Complex Math Functions", "group__group_cmplx_math.html", null ],
    [ "Filtering Functions", "group__group_filters.html", null ],
    [ "Matrix Functions", "group__group_matrix.html", null ],
    [ "Transform Functions", "group__group_transforms.html", null ],
    [ "Controller Functions", "group__group_controller.html", "group__group_controller" ],
    [ "Statistics Functions", "group__group_stats.html", null ],
    [ "Support Functions", "group__group_support.html", null ],
    [ "Interpolation Functions", "group__group_interpolation.html", "group__group_interpolation" ],
    [ "Examples", "group__group_examples.html", null ],
    [ "Drivers Programable", "group___drivers___programable.html", "group___drivers___programable" ]
];