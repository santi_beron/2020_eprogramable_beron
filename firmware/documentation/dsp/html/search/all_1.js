var searchData=
[
  ['a0_1',['A0',['../structarm__pid__instance__q15.html#ad77f3a2823c7f96de42c92a3fbf3246b',1,'arm_pid_instance_q15::A0()'],['../structarm__pid__instance__q31.html#aa5332635ce9c7078cdb4c1ecf442eadd',1,'arm_pid_instance_q31::A0()'],['../structarm__pid__instance__f32.html#ad7b0bed64915d0a25a3409fa2dc45556',1,'arm_pid_instance_f32::A0()']]],
  ['a1_2',['A1',['../structarm__pid__instance__q31.html#a2f7492bd6fb92fae5e2de7fbbec39b0e',1,'arm_pid_instance_q31::A1()'],['../structarm__pid__instance__f32.html#a7def89571c50f7137a213326a396e560',1,'arm_pid_instance_f32::A1()']]],
  ['a2_3',['A2',['../structarm__pid__instance__q31.html#a3e34537c53af4f9ad7bfffa4dff27c82',1,'arm_pid_instance_q31::A2()'],['../structarm__pid__instance__f32.html#a155acf642ba2f521869f19d694cd7fa0',1,'arm_pid_instance_f32::A2()']]],
  ['analog_5finput_5fconfig_4',['analog_input_config',['../structanalog__input__config.html',1,'']]],
  ['analog_20io_5',['Analog IO',['../group___analog___i_o.html',1,'']]],
  ['analoginputinit_6',['AnalogInputInit',['../group___analog___i_o.html#ga75e49898ce3cf18d67e4d463c6e2a8de',1,'AnalogInputInit(analog_input_config *inputs):&#160;analog_io.c'],['../group___analog___i_o.html#ga75e49898ce3cf18d67e4d463c6e2a8de',1,'AnalogInputInit(analog_input_config *inputs):&#160;analog_io.c']]],
  ['analoginputreadpolling_7',['AnalogInputReadPolling',['../group___analog___i_o.html#ga1673192453e03e9fab4976534b20f3e9',1,'AnalogInputReadPolling(uint8_t channel, uint16_t *value):&#160;analog_io.c'],['../group___analog___i_o.html#ga1673192453e03e9fab4976534b20f3e9',1,'AnalogInputReadPolling(uint8_t channel, uint16_t *value):&#160;analog_io.c']]],
  ['analogoutputinit_8',['AnalogOutputInit',['../group___analog___i_o.html#gab57399e946247652a096a0e2d3a1b69a',1,'AnalogOutputInit(void):&#160;analog_io.c'],['../group___analog___i_o.html#gab57399e946247652a096a0e2d3a1b69a',1,'AnalogOutputInit(void):&#160;analog_io.c']]],
  ['analogoutputwrite_9',['AnalogOutputWrite',['../group___analog___i_o.html#ga464364f0790a3e7d1ab9d9e9c2d9092c',1,'AnalogOutputWrite(uint16_t value):&#160;analog_io.c'],['../group___analog___i_o.html#ga464364f0790a3e7d1ab9d9e9c2d9092c',1,'AnalogOutputWrite(uint16_t value):&#160;analog_io.c']]],
  ['analogstartconvertion_10',['AnalogStartConvertion',['../group___analog___i_o.html#ga551f68a70593d1b97e8c41a470707368',1,'AnalogStartConvertion(void):&#160;analog_io.c'],['../group___analog___i_o.html#ga551f68a70593d1b97e8c41a470707368',1,'AnalogStartConvertion(void):&#160;analog_io.c']]],
  ['arm_5fbilinear_5finterp_5ff32_11',['arm_bilinear_interp_f32',['../group___bilinear_interpolate.html#gaba67d2ba2acbd428832f106de29b8627',1,'arm_math.h']]],
  ['arm_5fbilinear_5finterp_5finstance_5ff32_12',['arm_bilinear_interp_instance_f32',['../structarm__bilinear__interp__instance__f32.html',1,'']]],
  ['arm_5fbilinear_5finterp_5finstance_5fq15_13',['arm_bilinear_interp_instance_q15',['../structarm__bilinear__interp__instance__q15.html',1,'']]],
  ['arm_5fbilinear_5finterp_5finstance_5fq31_14',['arm_bilinear_interp_instance_q31',['../structarm__bilinear__interp__instance__q31.html',1,'']]],
  ['arm_5fbilinear_5finterp_5finstance_5fq7_15',['arm_bilinear_interp_instance_q7',['../structarm__bilinear__interp__instance__q7.html',1,'']]],
  ['arm_5fbilinear_5finterp_5fq15_16',['arm_bilinear_interp_q15',['../group___bilinear_interpolate.html#ga5f9b61bcae6d9295d926810cf398dd53',1,'arm_math.h']]],
  ['arm_5fbilinear_5finterp_5fq31_17',['arm_bilinear_interp_q31',['../group___bilinear_interpolate.html#ga79d374eda5d39e220b0423ba3cb90c6e',1,'arm_math.h']]],
  ['arm_5fbilinear_5finterp_5fq7_18',['arm_bilinear_interp_q7',['../group___bilinear_interpolate.html#ga0a986d4a01039914a9d6e81e9a4ccda0',1,'arm_math.h']]],
  ['arm_5fbiquad_5fcas_5fdf1_5f32x64_5fins_5fq31_19',['arm_biquad_cas_df1_32x64_ins_q31',['../structarm__biquad__cas__df1__32x64__ins__q31.html',1,'']]],
  ['arm_5fbiquad_5fcascade_5fdf2t_5finstance_5ff32_20',['arm_biquad_cascade_df2T_instance_f32',['../structarm__biquad__cascade__df2_t__instance__f32.html',1,'']]],
  ['arm_5fbiquad_5fcascade_5fdf2t_5finstance_5ff64_21',['arm_biquad_cascade_df2T_instance_f64',['../structarm__biquad__cascade__df2_t__instance__f64.html',1,'']]],
  ['arm_5fbiquad_5fcascade_5fstereo_5fdf2t_5finstance_5ff32_22',['arm_biquad_cascade_stereo_df2T_instance_f32',['../structarm__biquad__cascade__stereo__df2_t__instance__f32.html',1,'']]],
  ['arm_5fbiquad_5fcasd_5fdf1_5finst_5ff32_23',['arm_biquad_casd_df1_inst_f32',['../structarm__biquad__casd__df1__inst__f32.html',1,'']]],
  ['arm_5fbiquad_5fcasd_5fdf1_5finst_5fq15_24',['arm_biquad_casd_df1_inst_q15',['../structarm__biquad__casd__df1__inst__q15.html',1,'']]],
  ['arm_5fbiquad_5fcasd_5fdf1_5finst_5fq31_25',['arm_biquad_casd_df1_inst_q31',['../structarm__biquad__casd__df1__inst__q31.html',1,'']]],
  ['arm_5fcfft_5finstance_5ff32_26',['arm_cfft_instance_f32',['../structarm__cfft__instance__f32.html',1,'']]],
  ['arm_5fcfft_5finstance_5fq15_27',['arm_cfft_instance_q15',['../structarm__cfft__instance__q15.html',1,'']]],
  ['arm_5fcfft_5finstance_5fq31_28',['arm_cfft_instance_q31',['../structarm__cfft__instance__q31.html',1,'']]],
  ['arm_5fcfft_5fradix2_5finstance_5ff32_29',['arm_cfft_radix2_instance_f32',['../structarm__cfft__radix2__instance__f32.html',1,'']]],
  ['arm_5fcfft_5fradix2_5finstance_5fq15_30',['arm_cfft_radix2_instance_q15',['../structarm__cfft__radix2__instance__q15.html',1,'']]],
  ['arm_5fcfft_5fradix2_5finstance_5fq31_31',['arm_cfft_radix2_instance_q31',['../structarm__cfft__radix2__instance__q31.html',1,'']]],
  ['arm_5fcfft_5fradix4_5finstance_5ff32_32',['arm_cfft_radix4_instance_f32',['../structarm__cfft__radix4__instance__f32.html',1,'']]],
  ['arm_5fcfft_5fradix4_5finstance_5fq15_33',['arm_cfft_radix4_instance_q15',['../structarm__cfft__radix4__instance__q15.html',1,'']]],
  ['arm_5fcfft_5fradix4_5finstance_5fq31_34',['arm_cfft_radix4_instance_q31',['../structarm__cfft__radix4__instance__q31.html',1,'']]],
  ['arm_5fclarke_5ff32_35',['arm_clarke_f32',['../group__clarke.html#gabbfa27c68837d22f03d6c6259569caa8',1,'arm_math.h']]],
  ['arm_5fclarke_5fq31_36',['arm_clarke_q31',['../group__clarke.html#ga0c69ebd2855ce6d34008eff808881e78',1,'arm_math.h']]],
  ['arm_5fdct4_5finstance_5ff32_37',['arm_dct4_instance_f32',['../structarm__dct4__instance__f32.html',1,'']]],
  ['arm_5fdct4_5finstance_5fq15_38',['arm_dct4_instance_q15',['../structarm__dct4__instance__q15.html',1,'']]],
  ['arm_5fdct4_5finstance_5fq31_39',['arm_dct4_instance_q31',['../structarm__dct4__instance__q31.html',1,'']]],
  ['arm_5ffir_5fdecimate_5finstance_5ff32_40',['arm_fir_decimate_instance_f32',['../structarm__fir__decimate__instance__f32.html',1,'']]],
  ['arm_5ffir_5fdecimate_5finstance_5fq15_41',['arm_fir_decimate_instance_q15',['../structarm__fir__decimate__instance__q15.html',1,'']]],
  ['arm_5ffir_5fdecimate_5finstance_5fq31_42',['arm_fir_decimate_instance_q31',['../structarm__fir__decimate__instance__q31.html',1,'']]],
  ['arm_5ffir_5finstance_5ff32_43',['arm_fir_instance_f32',['../structarm__fir__instance__f32.html',1,'']]],
  ['arm_5ffir_5finstance_5fq15_44',['arm_fir_instance_q15',['../structarm__fir__instance__q15.html',1,'']]],
  ['arm_5ffir_5finstance_5fq31_45',['arm_fir_instance_q31',['../structarm__fir__instance__q31.html',1,'']]],
  ['arm_5ffir_5finstance_5fq7_46',['arm_fir_instance_q7',['../structarm__fir__instance__q7.html',1,'']]],
  ['arm_5ffir_5finterpolate_5finstance_5ff32_47',['arm_fir_interpolate_instance_f32',['../structarm__fir__interpolate__instance__f32.html',1,'']]],
  ['arm_5ffir_5finterpolate_5finstance_5fq15_48',['arm_fir_interpolate_instance_q15',['../structarm__fir__interpolate__instance__q15.html',1,'']]],
  ['arm_5ffir_5finterpolate_5finstance_5fq31_49',['arm_fir_interpolate_instance_q31',['../structarm__fir__interpolate__instance__q31.html',1,'']]],
  ['arm_5ffir_5flattice_5finstance_5ff32_50',['arm_fir_lattice_instance_f32',['../structarm__fir__lattice__instance__f32.html',1,'']]],
  ['arm_5ffir_5flattice_5finstance_5fq15_51',['arm_fir_lattice_instance_q15',['../structarm__fir__lattice__instance__q15.html',1,'']]],
  ['arm_5ffir_5flattice_5finstance_5fq31_52',['arm_fir_lattice_instance_q31',['../structarm__fir__lattice__instance__q31.html',1,'']]],
  ['arm_5ffir_5fsparse_5finstance_5ff32_53',['arm_fir_sparse_instance_f32',['../structarm__fir__sparse__instance__f32.html',1,'']]],
  ['arm_5ffir_5fsparse_5finstance_5fq15_54',['arm_fir_sparse_instance_q15',['../structarm__fir__sparse__instance__q15.html',1,'']]],
  ['arm_5ffir_5fsparse_5finstance_5fq31_55',['arm_fir_sparse_instance_q31',['../structarm__fir__sparse__instance__q31.html',1,'']]],
  ['arm_5ffir_5fsparse_5finstance_5fq7_56',['arm_fir_sparse_instance_q7',['../structarm__fir__sparse__instance__q7.html',1,'']]],
  ['arm_5fiir_5flattice_5finstance_5ff32_57',['arm_iir_lattice_instance_f32',['../structarm__iir__lattice__instance__f32.html',1,'']]],
  ['arm_5fiir_5flattice_5finstance_5fq15_58',['arm_iir_lattice_instance_q15',['../structarm__iir__lattice__instance__q15.html',1,'']]],
  ['arm_5fiir_5flattice_5finstance_5fq31_59',['arm_iir_lattice_instance_q31',['../structarm__iir__lattice__instance__q31.html',1,'']]],
  ['arm_5finv_5fclarke_5ff32_60',['arm_inv_clarke_f32',['../group__inv__clarke.html#ga945eb24e625a57c7c3be8a6e655646e3',1,'arm_math.h']]],
  ['arm_5finv_5fclarke_5fq31_61',['arm_inv_clarke_q31',['../group__inv__clarke.html#ga50768ebd8b71e8988dbb804cc03a742d',1,'arm_math.h']]],
  ['arm_5finv_5fpark_5ff32_62',['arm_inv_park_f32',['../group__inv__park.html#ga7ca3a87a0954ed8c9ed5a2e6f1c64f30',1,'arm_math.h']]],
  ['arm_5finv_5fpark_5fq31_63',['arm_inv_park_q31',['../group__inv__park.html#ga6e00d7320aa4a85686716af8b763e08a',1,'arm_math.h']]],
  ['arm_5flinear_5finterp_5ff32_64',['arm_linear_interp_f32',['../group___linear_interpolate.html#ga790bbc697724ee432f840cfebbbae1f4',1,'arm_math.h']]],
  ['arm_5flinear_5finterp_5finstance_5ff32_65',['arm_linear_interp_instance_f32',['../structarm__linear__interp__instance__f32.html',1,'']]],
  ['arm_5flinear_5finterp_5fq15_66',['arm_linear_interp_q15',['../group___linear_interpolate.html#ga7e5d633c26edd82e009517cd2347fb00',1,'arm_math.h']]],
  ['arm_5flinear_5finterp_5fq31_67',['arm_linear_interp_q31',['../group___linear_interpolate.html#gaad59cea673ab358888075b040bacc71f',1,'arm_math.h']]],
  ['arm_5flinear_5finterp_5fq7_68',['arm_linear_interp_q7',['../group___linear_interpolate.html#gaab750789ec9230e65c3bf544029d246f',1,'arm_math.h']]],
  ['arm_5flms_5finstance_5ff32_69',['arm_lms_instance_f32',['../structarm__lms__instance__f32.html',1,'']]],
  ['arm_5flms_5finstance_5fq15_70',['arm_lms_instance_q15',['../structarm__lms__instance__q15.html',1,'']]],
  ['arm_5flms_5finstance_5fq31_71',['arm_lms_instance_q31',['../structarm__lms__instance__q31.html',1,'']]],
  ['arm_5flms_5fnorm_5finstance_5ff32_72',['arm_lms_norm_instance_f32',['../structarm__lms__norm__instance__f32.html',1,'']]],
  ['arm_5flms_5fnorm_5finstance_5fq15_73',['arm_lms_norm_instance_q15',['../structarm__lms__norm__instance__q15.html',1,'']]],
  ['arm_5flms_5fnorm_5finstance_5fq31_74',['arm_lms_norm_instance_q31',['../structarm__lms__norm__instance__q31.html',1,'']]],
  ['arm_5fmatrix_5finstance_5ff32_75',['arm_matrix_instance_f32',['../structarm__matrix__instance__f32.html',1,'']]],
  ['arm_5fmatrix_5finstance_5ff64_76',['arm_matrix_instance_f64',['../structarm__matrix__instance__f64.html',1,'']]],
  ['arm_5fmatrix_5finstance_5fq15_77',['arm_matrix_instance_q15',['../structarm__matrix__instance__q15.html',1,'']]],
  ['arm_5fmatrix_5finstance_5fq31_78',['arm_matrix_instance_q31',['../structarm__matrix__instance__q31.html',1,'']]],
  ['arm_5fpark_5ff32_79',['arm_park_f32',['../group__park.html#gae24bf07174d3b8dddcf30d8c8d8e7fbb',1,'arm_math.h']]],
  ['arm_5fpark_5fq31_80',['arm_park_q31',['../group__park.html#ga5630ed3715091f2795ee10df34672523',1,'arm_math.h']]],
  ['arm_5fpid_5ff32_81',['arm_pid_f32',['../group___p_i_d.html#ga9380d08046dd7ec2f5015f569206b9ce',1,'arm_math.h']]],
  ['arm_5fpid_5finstance_5ff32_82',['arm_pid_instance_f32',['../structarm__pid__instance__f32.html',1,'']]],
  ['arm_5fpid_5finstance_5fq15_83',['arm_pid_instance_q15',['../structarm__pid__instance__q15.html',1,'']]],
  ['arm_5fpid_5finstance_5fq31_84',['arm_pid_instance_q31',['../structarm__pid__instance__q31.html',1,'']]],
  ['arm_5fpid_5fq15_85',['arm_pid_q15',['../group___p_i_d.html#gad466471a6b0f8fc570b8b8fc34ac79fa',1,'arm_math.h']]],
  ['arm_5fpid_5fq31_86',['arm_pid_q31',['../group___p_i_d.html#ga82bc0813c007d50ce308b9b0bf3c76e2',1,'arm_math.h']]],
  ['arm_5frfft_5ffast_5finstance_5ff32_87',['arm_rfft_fast_instance_f32',['../structarm__rfft__fast__instance__f32.html',1,'']]],
  ['arm_5frfft_5finstance_5ff32_88',['arm_rfft_instance_f32',['../structarm__rfft__instance__f32.html',1,'']]],
  ['arm_5frfft_5finstance_5fq15_89',['arm_rfft_instance_q15',['../structarm__rfft__instance__q15.html',1,'']]],
  ['arm_5frfft_5finstance_5fq31_90',['arm_rfft_instance_q31',['../structarm__rfft__instance__q31.html',1,'']]],
  ['arm_5fsqrt_5ff32_91',['arm_sqrt_f32',['../group___s_q_r_t.html#ga697d82c2747a3302cf44e7c9583da2e8',1,'arm_math.h']]],
  ['arm_5fsqrt_5fq15_92',['arm_sqrt_q15',['../group___s_q_r_t.html#ga5abe5ca724f3e15849662b03752c1238',1,'arm_math.h']]],
  ['arm_5fsqrt_5fq31_93',['arm_sqrt_q31',['../group___s_q_r_t.html#ga119e25831e141d734d7ef10636670058',1,'arm_math.h']]]
];
