var searchData=
[
  ['statistics_20functions_246',['Statistics Functions',['../group__group_stats.html',1,'']]],
  ['support_20functions_247',['Support Functions',['../group__group_support.html',1,'']]],
  ['serial_5fconfig_248',['serial_config',['../structserial__config.html',1,'']]],
  ['sint_249',['Sint',['../structarm__rfft__fast__instance__f32.html#a37419ababdfb3151b1891ae6bcd21012',1,'arm_rfft_fast_instance_f32']]],
  ['square_20root_250',['Square Root',['../group___s_q_r_t.html',1,'']]],
  ['state_251',['state',['../structarm__pid__instance__q15.html#a4a3f0a878b5b6b055e3478a2f244cd30',1,'arm_pid_instance_q15::state()'],['../structarm__pid__instance__q31.html#a228e4a64da6014844a0a671a1fa391d4',1,'arm_pid_instance_q31::state()'],['../structarm__pid__instance__f32.html#afd394e1e52fb1d526aa472c83b8f2464',1,'arm_pid_instance_f32::state()']]],
  ['stateindex_252',['stateIndex',['../structarm__fir__sparse__instance__f32.html#a57585aeca9dc8686e08df2865375a86d',1,'arm_fir_sparse_instance_f32::stateIndex()'],['../structarm__fir__sparse__instance__q31.html#a557ed9d477e76e4ad2019344f19f568a',1,'arm_fir_sparse_instance_q31::stateIndex()'],['../structarm__fir__sparse__instance__q15.html#a89487f28cab52637426024005e478985',1,'arm_fir_sparse_instance_q15::stateIndex()'],['../structarm__fir__sparse__instance__q7.html#a2d2e65473fe3a3f2b953b4e0b60824df',1,'arm_fir_sparse_instance_q7::stateIndex()']]],
  ['switch_253',['Switch',['../group___switch.html',1,'']]],
  ['switchactivint_254',['SwitchActivInt',['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t tec, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t sw, void *ptr_int_func):&#160;switch.c']]],
  ['switches_255',['SWITCHES',['../group___switch.html#gaa87203a5637fb4759a378b579aaebff6',1,'switch.h']]],
  ['switchesactivgroupint_256',['SwitchesActivGroupInt',['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t tecs, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t switchs, void *ptr_int_func):&#160;switch.c']]],
  ['switchesinit_257',['SwitchesInit',['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c'],['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c']]],
  ['switchesread_258',['SwitchesRead',['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c'],['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c']]],
  ['systemclock_259',['Systemclock',['../group___systemclock.html',1,'']]],
  ['systemclockinit_260',['SystemClockInit',['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c'],['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c']]]
];
