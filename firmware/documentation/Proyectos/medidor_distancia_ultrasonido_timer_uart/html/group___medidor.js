var group___medidor =
[
    [ "DISTANCIA_0", "group___medidor.html#ga2ad1061d6b6ad4816cd012423c921e6b", null ],
    [ "DISTANCIA_1", "group___medidor.html#ga657140fd19e5ffb839a182341ee87c28", null ],
    [ "DISTANCIA_2", "group___medidor.html#ga289980e3b2611b848a386068981720d2", null ],
    [ "DISTANCIA_3", "group___medidor.html#ga840574b0b391beb7bac8766e9512636f", null ],
    [ "ActualizarMuestra", "group___medidor.html#gad22e608c4916efecf6336c76b0f8b5ee", null ],
    [ "ApagarLeds", "group___medidor.html#ga7e2afcb22f7a75befe4efc34a57539d1", null ],
    [ "DoSerial", "group___medidor.html#ga24d89f63ec3190a26f9aa3a268cc5027", null ],
    [ "EnviarInformacion", "group___medidor.html#ga7b064d2b4df5d564c781b90ff6e0c7b9", null ],
    [ "ISR_Tecla1", "group___medidor.html#ga5171f63b67955fd3278891346f5a1178", null ],
    [ "ISR_Tecla2", "group___medidor.html#ga274d211f77d9a7a82a577172a17189ac", null ],
    [ "MostrarDistancia", "group___medidor.html#gac19fe46b2d4df242f077b1cdfab442fd", null ],
    [ "activo", "group___medidor.html#ga140b48a7f0058f492e01eb3af383753b", null ],
    [ "distancia", "group___medidor.html#ga0c9fa70e5523b4b389b608dfbad18d7b", null ],
    [ "hold", "group___medidor.html#ga1691a4063399c74f6a751e402020b354", null ]
];