var searchData=
[
  ['alfa_1',['alfa',['../group___proyecto__4.html#ga5bf563dfc55db9dac98ac73ed7a8df92',1,'proyecto4.c']]],
  ['analog_5finput_5fconfig_2',['analog_input_config',['../structanalog__input__config.html',1,'']]],
  ['analog_20io_3',['Analog IO',['../group___analog___i_o.html',1,'']]],
  ['analoginputinit_4',['AnalogInputInit',['../group___analog___i_o.html#ga75e49898ce3cf18d67e4d463c6e2a8de',1,'AnalogInputInit(analog_input_config *inputs):&#160;analog_io.c'],['../group___analog___i_o.html#ga75e49898ce3cf18d67e4d463c6e2a8de',1,'AnalogInputInit(analog_input_config *inputs):&#160;analog_io.c']]],
  ['analoginputreadpolling_5',['AnalogInputReadPolling',['../group___analog___i_o.html#ga1673192453e03e9fab4976534b20f3e9',1,'AnalogInputReadPolling(uint8_t channel, uint16_t *value):&#160;analog_io.c'],['../group___analog___i_o.html#ga1673192453e03e9fab4976534b20f3e9',1,'AnalogInputReadPolling(uint8_t channel, uint16_t *value):&#160;analog_io.c']]],
  ['analogoutputinit_6',['AnalogOutputInit',['../group___analog___i_o.html#gab57399e946247652a096a0e2d3a1b69a',1,'AnalogOutputInit(void):&#160;analog_io.c'],['../group___analog___i_o.html#gab57399e946247652a096a0e2d3a1b69a',1,'AnalogOutputInit(void):&#160;analog_io.c']]],
  ['analogoutputwrite_7',['AnalogOutputWrite',['../group___analog___i_o.html#ga464364f0790a3e7d1ab9d9e9c2d9092c',1,'AnalogOutputWrite(uint16_t value):&#160;analog_io.c'],['../group___analog___i_o.html#ga464364f0790a3e7d1ab9d9e9c2d9092c',1,'AnalogOutputWrite(uint16_t value):&#160;analog_io.c']]],
  ['analogstartconvertion_8',['AnalogStartConvertion',['../group___analog___i_o.html#ga551f68a70593d1b97e8c41a470707368',1,'AnalogStartConvertion(void):&#160;analog_io.c'],['../group___analog___i_o.html#ga551f68a70593d1b97e8c41a470707368',1,'AnalogStartConvertion(void):&#160;analog_io.c']]]
];
