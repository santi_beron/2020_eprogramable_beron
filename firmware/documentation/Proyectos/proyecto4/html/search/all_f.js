var searchData=
[
  ['template_132',['Template',['../index.html',1,'']]],
  ['tiempoadc_133',['tiempoADC',['../group___proyecto__4.html#ga5e5327be86350d4bc8ea2dff28fbbf33',1,'proyecto4.c']]],
  ['tiempodac_134',['tiempoDAC',['../group___proyecto__4.html#gac73b193cc1ba1557c924db0e0833d5c3',1,'proyecto4.c']]],
  ['timer_135',['timer',['../structtimer__config.html#ab42a22a518af439f16e4d09e51aa2553',1,'timer_config::timer()'],['../group___timer.html',1,'(Global Namespace)']]],
  ['timer_5fconfig_136',['timer_config',['../structtimer__config.html',1,'']]],
  ['timerinit_137',['TimerInit',['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c'],['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c']]],
  ['timerreset_138',['TimerReset',['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c']]],
  ['timerstart_139',['TimerStart',['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c']]],
  ['timerstop_140',['TimerStop',['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c'],['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c']]],
  ['true_141',['true',['../group___bool.html#ga41f9c5fb8b08eb5dc3edce4dcb37fee7',1,'true():&#160;bool.h'],['../group___bool.html#ga41f9c5fb8b08eb5dc3edce4dcb37fee7',1,'true():&#160;bool.h']]]
];
