var searchData=
[
  ['hardconfig_89',['HardConfig',['../group___proyecto__4.html#ga86f2fe4f0faf3116962e9375ecea23d5',1,'proyecto4.c']]],
  ['hc_5fsr4_90',['HC_SR4',['../group___h_c___s_r4.html',1,'']]],
  ['hcsr04deinit_91',['HcSr04Deinit',['../group___h_c___s_r4.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04gettimeus_92',['HcSr04GetTimeUs',['../group___h_c___s_r4.html#ga53ceb1b5db0cffb0c592aa61ad4fd54b',1,'HcSr04GetTimeUs(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#ga53ceb1b5db0cffb0c592aa61ad4fd54b',1,'HcSr04GetTimeUs(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04init_93',['HcSr04Init',['../group___h_c___s_r4.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04readdistancecentimeters_94',['HcSr04ReadDistanceCentimeters',['../group___h_c___s_r4.html#gaa1b3e3a72f081db98eafa97000197a79',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#gaa1b3e3a72f081db98eafa97000197a79',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c']]],
  ['hcsr04readdistanceinches_95',['HcSr04ReadDistanceInches',['../group___h_c___s_r4.html#gac94cf11dbb224fead4118c8fbc7199c5',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#gac94cf11dbb224fead4118c8fbc7199c5',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c']]],
  ['hwpin_96',['hwPin',['../structdigital_i_o.html#a93d2e4a48daa464205632175fdb7288c',1,'digitalIO']]],
  ['hwport_97',['hwPort',['../structdigital_i_o.html#a79691c4619ba92dbf4859aaa6e006531',1,'digitalIO']]]
];
