var searchData=
[
  ['hc_5fsr4_75',['HC_SR4',['../group___h_c___s_r4.html',1,'']]],
  ['hcsr04deinit_76',['HcSr04Deinit',['../group___h_c___s_r4.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04gettimeus_77',['HcSr04GetTimeUs',['../group___h_c___s_r4.html#ga53ceb1b5db0cffb0c592aa61ad4fd54b',1,'HcSr04GetTimeUs(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#ga53ceb1b5db0cffb0c592aa61ad4fd54b',1,'HcSr04GetTimeUs(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04init_78',['HcSr04Init',['../group___h_c___s_r4.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04readdistancecentimeters_79',['HcSr04ReadDistanceCentimeters',['../group___h_c___s_r4.html#gaa1b3e3a72f081db98eafa97000197a79',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#gaa1b3e3a72f081db98eafa97000197a79',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c']]],
  ['hcsr04readdistanceinches_80',['HcSr04ReadDistanceInches',['../group___h_c___s_r4.html#gac94cf11dbb224fead4118c8fbc7199c5',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c'],['../group___h_c___s_r4.html#gac94cf11dbb224fead4118c8fbc7199c5',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c']]],
  ['hold_81',['hold',['../group___medidor.html#ga1691a4063399c74f6a751e402020b354',1,'medidor_distancia_ultrasonido.c']]],
  ['hwpin_82',['hwPin',['../structdigital_i_o.html#a93d2e4a48daa464205632175fdb7288c',1,'digitalIO']]],
  ['hwport_83',['hwPort',['../structdigital_i_o.html#a79691c4619ba92dbf4859aaa6e006531',1,'digitalIO']]]
];
