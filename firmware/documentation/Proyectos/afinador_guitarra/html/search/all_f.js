var searchData=
[
  ['sample_5ffrec_5fus_129',['SAMPLE_FREC_US',['../group___afinador___guitarra.html#gabc2d9f58daa09660db40648ad2e2d2f8',1,'afinador_guitarra.c']]],
  ['serial_5fconfig_130',['serial_config',['../structserial__config.html',1,'']]],
  ['signal_131',['signal',['../group___afinador___guitarra.html#ga4c7662d5b2b3f674ef4ebeac85d41eda',1,'afinador_guitarra.c']]],
  ['startconvertion_132',['StartConvertion',['../group___afinador___guitarra.html#ga6977ddb9661da104af90357cd4610037',1,'afinador_guitarra.c']]],
  ['switch_133',['Switch',['../group___switch.html',1,'']]],
  ['switchactivint_134',['SwitchActivInt',['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t tec, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t sw, void *ptr_int_func):&#160;switch.c']]],
  ['switches_135',['SWITCHES',['../group___switch.html#gaa87203a5637fb4759a378b579aaebff6',1,'switch.h']]],
  ['switchesactivgroupint_136',['SwitchesActivGroupInt',['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t tecs, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t switchs, void *ptr_int_func):&#160;switch.c']]],
  ['switchesinit_137',['SwitchesInit',['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c'],['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c']]],
  ['switchesread_138',['SwitchesRead',['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c'],['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c']]],
  ['systemclock_139',['Systemclock',['../group___systemclock.html',1,'']]],
  ['systemclockinit_140',['SystemClockInit',['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c'],['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c']]]
];
