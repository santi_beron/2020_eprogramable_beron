/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup HC_SR4
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Santiago Beron
 *
 * Este driver provee las funciones necesarias para operar el sensor ultrasonido
 *
 * @note
 *
 * @section changelog
 *
 * |   Fecha    | Descripcion                                    |
 * |:----------:|:-----------------------------------------------|
 * | 24/08/2020 | Creacion del documento		                         |
 *
 */

#ifndef _HC_SR4_H
#define _HC_SR4_H
/*==================[inclusions]=============================================*/

#include <stdint.h>
#include "gpio.h"
#include "delay.h"


/*==================[macros and definitions]=================================*/

/**
 * @brief inicializa sensor
 * @param[in] echo puerto de la EDU-CIAA correspondiente al periférico ECHO
 * @param[in] trigger puerto de la EDU-CIAA correspondiente al periférico TRIGGER
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/**
 * @brief devuelve la distancia en centímetros
 * @return valor de la medicion en centimetros
 */
int16_t HcSr04ReadDistanceCentimeters(void);
/**
 * @brief devuelve la distancia en pulgadas
 * @return valor de la medicion en pulgadas
 */
int16_t HcSr04ReadDistanceInches(void);

/**
 * @brief Deinicializacion de los puertos utilizados de la EDU-CIAA
 * @param[in] echo puerto de la EDU-CIAA correspondiente al periférico ECHO
 * @param[in] trigger puerto de la EDU-CIAA correspondiente al periférico TRIGGER
 */

bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/**
 * @brief Envia la señal de ECHO y calcula el tiempo correspondiente al ancho del pulso de la señal que recibe TRIGGER
 * @param[in] echo puerto de la EDU-CIAA correspondiente al periférico ECHO
 * @param[in] trigger puerto de la EDU-CIAA correspondiente al periférico TRIGGER
 */
void HcSr04GetTimeUs(gpio_t echo, gpio_t trigger);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

