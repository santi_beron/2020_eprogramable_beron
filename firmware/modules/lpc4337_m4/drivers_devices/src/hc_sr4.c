/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Beron Santiago
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *

 */

/**
 * @brief Este driver provee las funciones necesarias para operar el sensor ultrasonido
 **/

/*
 * Initials     Name
 * ---------------------------
 * SB		    Santiago Beron
 */


/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
uint16_t contador; /**< Variable auxiliar que guarda el tiempo correspondiente al ancho del pulso de la señal que recibe TRIGGER  */

/*==================[macros and definitions]=================================*/

/**
 * @brief Inicializa sensor
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);

	//GPIOOff(echo);
	//GPIOOff(trigger);

	return true;
}

/**
 * @brief  Devuelve la distancia en centímetros
 */
int16_t HcSr04ReadDistanceCentimeters(void)
{
	return (contador / 27) ;
}

/**
 * @brief  Devuelve la distancia en pulgadas
 */
int16_t HcSr04ReadDistanceInches(void)
{

	return (contador / 148) ;

}

/**
 * @brief  Deinicializacion de los puertos utilizados de la EDU-CIAA
 */

bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{
	GPIODeInit();
	return true;

}

/**
 * @brief  Manda la señal de ECHO y calcula el tiempo correspondiente al ancho del pulso de la señal que recibe TRIGGER
 */

void HcSr04GetTimeUs(gpio_t echo, gpio_t trigger)
{

	contador=0;

	GPIOOn(trigger);
	DelayUs(10);
	GPIOOff(trigger);


	while(GPIORead(echo)==0)
		{



		}

	while(GPIORead(echo)==1)
	{

		DelayUs(1);
		contador++;

	}


}


/*==================[internal functions declaration]=========================*/


/*==================[end of file]============================================*/

