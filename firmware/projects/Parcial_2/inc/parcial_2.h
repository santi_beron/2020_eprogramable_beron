/* Copyright 2020,
 * Santiago Beron
 * santiib98@outlook.com.ar
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * SB			Santiago Beron
 */

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * Programa que mide el voltaje generado por el punto medio de un potenciómetro conectado
 * a la entrada CH2. La frecuencia de muestreo es de 50 Hz. Se envía el valor promedio por
 * puerto serie de las muestras del último segundo.
 *
 *
 * <a href="'''">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Potenciometro			|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	Extremo 1	         	| 	 VDDA		|
 * | 	Extremo 2	 	        | 	 GNDA 	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                                    |
 * |:----------:|:---------------------------------------------------------------|
 * | 02/11/2020 | Implementación del programa								     |
 *
 * @author Santiago Beron
 *
 */


#ifndef _PARCIAL_2_H
#define _PARCIAL_2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

