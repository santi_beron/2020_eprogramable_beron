/** \addtogroup Proyectos
 ** @{ */
/** \addtogroup Parcial_2
 ** @{ */

/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Santiago Beron
 *
 * Revisión:
 * 2-11-20: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Ejercicio 2 del parcial
*
* @section genDesc Descripción general
*
* Programa que mide el voltaje generado por el punto medio de un potenciómetro conectado
* a la entrada CH2. La frecuencia de muestreo es de 50 Hz. Se envía el valor promedio por
* puerto serie de las muestras del último segundo.
*
*/



/*==================[inclusions]=============================================*/
#include "parcial_2.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/
/** @def AD_FREC
*	@brief Define la frecuencia de muestreo
*/
#define AD_FREC 1000/50
/** @def CANT_FREC
*	@brief Define la cantidad de muestras que hay en un segundo
*/
#define CANT_MUESTRAS 50

/*==================[internal functions declaration]=========================*/
/**
 * @brief Función que arranca la conversión
 */
void Muestrear(void);
/**
 * @brief Función que obtiene el voltaje
 * generado por el punto medio de un potenciómetro conectado a la entrada CH2
 */
void ObtenerVoltaje(void);

/**
 * @brief Función que inicializa los drivers utilizados
 */
void HardConfig();
/*==================[internal data definition]===============================*/



serial_config miPuerto={SERIAL_PORT_PC,115200, NULL}; /**<Estructura para configurar el puerto serie*/
timer_config frec_muestreo={TIMER_A,AD_FREC,Muestrear};/**<Estructura para configurar el TIMER_A*/
analog_input_config miADC={CH2, AINPUTS_SINGLE_READ,ObtenerVoltaje}; /**<Estructura para configurar el conversor*/

bool new_window=false; /**< Booleano que se pone en true cada vez que pasa un segundo */
uint8_t i_muestras; /**< Variable que cuanta cuantas muestras pasan hasta llegar al segundo */
uint16_t dato; /**< Variable que guarda el valor de la conversión */
float voltaje; /**< Variable que guarda el voltaje generado */
float prom; /**< Variable que guarda el promedio del voltaje en el último segundo */


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


void ObtenerVoltaje(){

	//(50/3.3) escala adoptada. No se especifica en el parcial.
	AnalogInputRead(CH2, &dato);
	voltaje=(5000/3.3)*(3.3/1024)*dato;

   if(i_muestras<CANT_MUESTRAS)
	{
		prom=prom+voltaje;
	}

	else
	{
		prom=prom/CANT_MUESTRAS;
		new_window=true;
	}

}
void Muestrear(void)
{
	AnalogStartConvertion();
}

void HardConfig(){

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SystemClockInit();
	TimerInit(&frec_muestreo);
	TimerStart(TIMER_A);
	UartInit(&miPuerto);
	AnalogInputInit(&miADC);

}

int main(void)
{

    while(1)
    {

    	if(new_window==true)
    	{
    		UartSendString(SERIAL_PORT_PC, UartItoa(prom,10));
    		UartSendString(SERIAL_PORT_PC, "\r\n");
    		new_window=false;
    		prom=0;
    	}

	}

    
}

/*==================[end of file]============================================*/

