parcial_2

Programa que mide el votaje generado por el punto medio de un potenciómetro conectado a la entrada CH2. La frecuencia de muestreo es de 50 Hz. Se envía el valor promedio por
puerto serie de las muestras del último segundo.