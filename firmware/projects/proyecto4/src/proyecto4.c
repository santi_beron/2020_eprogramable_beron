/** \addtogroup Proyectos
 ** @{ */
/** \addtogroup Proyecto_4
 ** @{ */

/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Santiago Beron
 *
 * Revisión:
 * 18-10-20: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Medidor de distancia con sensor ultrasonido, interrupciones, timer y puerto serie
*
* @section genDesc Descripción general
*
 * Este programa convierte una señal digital de un ECG en una señal analógica. Además se ha implementado un
 * filtro digital pasa-bajo, aplicado sobre la señal de ECG y puede visualizarse en el graficador de puerto serie
 * la señal original y la señal filtrada. Al presionar las teclas de la EDU-CIAA se activa y desactiva el filtrado,
 * y se cambia la frecuencia de corte del filtro
*/



/*==================[inclusions]=============================================*/
#include "proyecto4.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"



/*==================[macros and definitions]=================================*/
/** @def PERIODO
*	@brief periodo de muestreo de la señal de ECG
*/
#define PERIODO 0.002

/*==================[internal data definition]===============================*/

const char ecg1[]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
}; /**< Vector que contiene los valores del ECG*/

bool filtrado=FALSE; /**< Variable cuyo estado indica si se realiza el filtrado*/
uint8_t muestra=0; /**< Índice para recorrer el vector de ECG*/
uint16_t valorADC=0; /**< Variable de 16 bits que guarda la muestra del conversor (de 10 bits)*/
uint8_t fc=100; /**< Frecuencia de corte del filtro*/
uint16_t valor_filtrado; /**< Variable que guarda el valor de la muestra filtrada*/
uint16_t valor_anterior=0; /**< Variable que guarda el valor de la muestra anterior filtrada*/
float alfa; /**< Variable requerida para aplicar el filtro*/
float RC; /**< Variable requerida para aplicar el filtro*/

/*==================[internal functions declaration]=========================*/

/**
 * @brief Función que aplica el filtro sobre el ECG
 */
uint16_t filtrar(uint16_t anterior,  uint16_t actual);

/**
 * @brief Función que arranca la conversión
 */
void DoTimerA();
/**
 * @brief Función que recrea el ECG, saca por el DAC los valores del vector de ECG
 */
void DoTimerB();
/**
 * @brief Función que envía por el puerto serie los valores del ECG y/o los valores
 * del ECG filtrado, si así se lo indica el estado de la variable filtrado
 */
void DoADC();
/**
 * @brief Función que activa el filtrado de la señal de ECG
 */
void FuncionTecla_1();
/**
 * @brief Función que desactiva el filtrado de la señal de ECG
 */
void FuncionTecla_2();
/**
 * @brief Función que disminuye el valor de la frecuencia de corte y actualiza
 * los valores de las variables del viltro: RC y alfa
 */
void FuncionTecla_3();
/**
 * @brief Función que aumenta el valor de la frecuencia de corte y actualiza
 * los valores de las variables del viltro: RC y alfa
 */
void FuncionTecla_4();
/**
 * @brief Función que inicializa los drivers utilizados
 */
void HardConfig();


/*==================[external data definition]===============================*/

/*Con 115200 baudios aseguro que termino de transmitir los valores antes de que se produzca
una nueva interrupción y tengo nuevos valores que enviar por la UART*/

serial_config miPuerto={SERIAL_PORT_PC,115200, NULL}; /**<Estructura para configurar el puerto serie*/
analog_input_config miADC={CH1, AINPUTS_SINGLE_READ,DoADC}; /**<Estructura para configurar el conversor*/
timer_config tiempoADC={TIMER_A,2,DoTimerA};/**<Estructura para configurar el TIMER_A*/
timer_config tiempoDAC={TIMER_B,4,DoTimerB};/**<Estructura para configurar el TIMER_B*/

/*==================[external functions definition]==========================*/

uint16_t filtrar(uint16_t anterior,  uint16_t actual){


	uint16_t salida_filtrada;
	salida_filtrada = anterior+alfa*(actual-anterior);
	return salida_filtrada;

}

void DoTimerA(void)
{
	AnalogStartConvertion();
}


void DoTimerB(void)
{
	AnalogOutputWrite(ecg1[muestra]);
	muestra++;

	//Cada vez que llego al final arranco desde el principio del vector

	if(muestra==231)
	{
		muestra=0;
	}
}

void DoADC(void)
{
	AnalogInputRead(CH1, &valorADC );

	if(filtrado){
		valor_filtrado=filtrar(valor_anterior, valorADC);
		valor_anterior=valor_filtrado;
		//ahora valor anterior es el valor filtrado de la pasada anterior
		UartSendString(SERIAL_PORT_PC, UartItoa(valor_filtrado,10));
		UartSendString(SERIAL_PORT_PC, ",");

	}

	/*UartSendString(SERIAL_PORT_PC, UartItoa(valorADC,10));
	Envio 0 para que la gráfica del ECG no se superponga con la gráfica del ECG
	filtrado y pueda así apreciarse el efecto del filtro
	*/

	UartSendString(SERIAL_PORT_PC, UartItoa(0,10));
	UartSendString(SERIAL_PORT_PC, "\r");
}

void FuncionTecla_1()
{
	filtrado=TRUE;
	//Para darle valor a alfa cuando se activa el filtrado por primera vez
	alfa=PERIODO/(1/2*3.14*fc+PERIODO);

}
void FuncionTecla_2()
{
	filtrado=FALSE;
}

void FuncionTecla_3()
{
	if(fc<245)
	{
		fc=fc+10;

	}
	RC=1/(2*3.14*fc);
	alfa=PERIODO/(RC+PERIODO);


}

void FuncionTecla_4()
{
	if(fc>10)
	{
		fc=fc-10;

	}
	RC=1/(2*3.14*fc);
	alfa=PERIODO/(RC+PERIODO);


}


void HardConfig()
{
	SystemClockInit();

	TimerInit(&tiempoADC);
	TimerInit(&tiempoDAC);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	LedsInit();


	UartInit(&miPuerto);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, FuncionTecla_1);
	SwitchActivInt(SWITCH_2, FuncionTecla_2);
	SwitchActivInt(SWITCH_3, FuncionTecla_3);
	SwitchActivInt(SWITCH_4, FuncionTecla_4);

	AnalogInputInit(&miADC);
	AnalogOutputInit();

}

int main(void){

	HardConfig();

    while(1)
    {


	}
    
	return 0;
}

/*==================[end of file]============================================*/

