/* Copyright 2020,
 * Santiago Beron
 * santiib98@outlook.com.ar
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * SB			Santiago Beron
 */

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * Este programa convierte una señal digital de un ECG en una señal analógica. Además se ha implementado un
 * filtro digital pasa-bajo, aplicado sobre la señal de ECG y puede visualizarse en el graficador de puerto serie
 * la señal original y la señal filtrada. Al presionar las teclas de la EDU-CIAA se activa y desactiva el filtrado,
 * y se cambia la frecuencia de corte del filtro.
 *
 * <a href="https://www.youtube.com/watch?v=gn0Ho5Jr2XQ">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |    ---		|   EDU-CIAA	|
 * |:----------:|:--------------|
 * | 	---	  	| 	   CH1		|
 * | 	---     | 	   DAC     	|

 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                                    |
 * |:----------:|:---------------------------------------------------------------|
 * | 07/10/2020 | Implementación del conversor y envío por puerto serie del ECG  |
 * | 19/09/2020	| Implementación del filtro							             |

 *
 * @author Santiago Beron
 *
 */

#ifndef _PROYECTO4_H
#define _PROYECTO4_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

