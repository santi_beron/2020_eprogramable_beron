/** \addtogroup Proyectos
 ** @{ */
/** \addtogroup Parcial_1
 ** @{ */

/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Santiago Beron
 *
 * Revisión:
 * 2-11-20: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Ejercicio 1 del parcial
*
* @section genDesc Descripción general
*
* Programa que aumenta la frecuencia de parpadeo del LED 1 según la distancia a la cual
* se encuentra el objeto
*
*
*/


/*==================[inclusions]=============================================*/
#include "parcial.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "timer.h"


/*==================[macros and definitions]=================================*/
/*==================[internal functions declaration]=========================*/

/**
 * @brief Función que obtiene la distancia a la cual se encuentra el objeto
 */
void ObtenerDistancia(void);

/**
 * @brief Función que hace parpadear el LED
 */
void ParpadearLed();

/**
 * @brief Función que inicializa los drivers utilizados
 */
void HardConfig();

/*==================[internal data definition]===============================*/

timer_config frec_led={TIMER_A,1,ObtenerDistancia};/**<Estructura para configurar el TIMER_A*/
/*Cada 1 ms actualizo la distancia a la cual se encuentra el objeto,
 * con lo cual actualizo la frecuencia de parpadeo. Hago coincider este valor de
 * actualización con la frec de parpadeo mas rapida
 */

uint8_t frecuencia; /**< Variable que indica la frecuencia de parpadeo del LED */


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void ParpadearLed(){

	LedOn(LED_1);
	DelayUs(frecuencia);
	LedOff(LED_1);
	DelayUs(frecuencia);

}

void ObtenerDistancia(void)
{
	HcSr04GetTimeUs(GPIO_T_FIL2,GPIO_T_FIL3);
	frecuencia=HcSr04ReadDistanceCentimeters();
	//frecuencia de parpadeo dada por la distancia en centimetros
	//a 1 cm parpadea cada 1 ms
	//a 20 cm parpadea dada 20 ms

}

void HardConfig(){

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2,	GPIO_T_FIL3);
	TimerInit(&frec_led);
	TimerStart(TIMER_A);

}

int main(void)
{

	HardConfig();

    while(1)
    {
    	ParpadearLed();
	}

    
}

/*==================[end of file]============================================*/

