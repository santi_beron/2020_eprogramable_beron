afinador_guitarra

Este programa  realiza el sensado, a través de un micrófono, de los sonidos provenientes
de las cuerdas de la guitarra. Realiza la conversión AD y le calcula a la señal resultante la TTF.
Una vez hecho esto, se encuentra el subíndice del valor máximo, que corresponde a la frecuencia con
la que sonó la cuerda. En función de esta frecuencia, se reconoce el número de cuerda y se prenden
los LEDS de la EDU-CIAA según la distancia de esta frecuencia respecto a la frecuencia de la
cuerda afinada. Cuando estas coinciden se prenden todos los LEDS.