/** \addtogroup Proyectos
 ** @{ */
/** \addtogroup Afinador_Guitarra
 ** @{ */

/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Santiago Beron
 *
 * Revisión:
 * 06-11-20: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Afinador de Guitarra
*
* @section genDesc Descripción general
*
*Este programa  realiza el sensado, a través de un micrófono, de los sonidos provenientes
*de las cuerdas de la guitarra. Realiza la conversión AD y le calcula a la señal resultante la TTF.
*Una vez hecho esto, se encuentra el subíndice del valor máximo, que corresponde a la frecuencia con
*la que sonó la cuerda. En función de esta frecuencia, se reconoce el número de cuerda y se prenden
*los LEDS de la EDU-CIAA según la distancia de esta frecuencia respecto a la frecuencia de la
*cuerda afinada. Cuando estas coinciden se prenden todos los LEDS.
*/



/*==================[inclusions]=============================================*/
#include "afinador_guitarra.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "analog_io.h"
#include "timer.h"
#include "fpu_init.h"
#include "uart.h"
#include "arm_math.h"
#include "arm_const_structs.h"

/*==================[macros and definitions]=================================*/

/** @def TOLERANCIA
*	@brief Valor que indica cuantos Hz por encima y por debajo de la frecuencia ideal de la cuerda
*	tomo para reconocer la cuerda
*/
#define TOLERANCIA 10
/** @def CANT_CUERDAS
*	@brief Cantidad de cuerdas de la guitarra
*/
#define CANT_CUERDAS 6

/** @def LIMITE_1
*	@brief Cantidad de Hz por encima y por debajo de la frecuencia ideal de la cuerda para considerar
*	afinada a la cuerda, prendiendo así todos los LEDS.
*/
#define LIMITE_1 2

/** @def LIMITE_2
*	@brief Cantidad de Hz por encima y por debajo de la frecuencia ideal de la cuerda para prender
*	el LED 1 y el LED 2. El LED 2 se enciende cuando la frecuencia de la cuerda se encuentra por debajo
*	de la ideal, y el LED 3 por encima de esta. En la implementación el rango de frecuencias para encender
*	los LEDS 2 y 3 se define considerando el valor de LIMITE_1 como límite inferior.
*
*/

#define LIMITE_2 4

/** @def LIMITE_3
*	@brief Cantidad de Hz por encima y por debajo de la frecuencia ideal de la cuerda para prender
*	el LED RGB y el LED 3. El LED RGB se enciende cuando la frecuencia de la cuerda se encuentra por debajo
*	de la ideal, y el LED 3 por encima de esta. En la implementación el rango de frecuencias para encender
*	los LEDS RGB y LED 3 se define considerando el valor de LIMITE_2 como límite inferior.
*
*/
#define LIMITE_3 10

/** @def ARM_MATH_CM4
 * Configuro libreria de procesamiento de señales
 */
#define ARM_MATH_CM4
/** @def __FPU_PRESENT
 *  Configuro libreria de procesamiento de señales
 */

#define __FPU_PRESENT 1

/** @def IFFT_FLAG
*	@brief Indica que se realiza la TTF directa (no la inversa)
*/
#define IFFT_FLAG 0

/** @def DO_BIT_REVERSE
*	@brief Habilita la inversión de bits de la salida
*/
#define DO_BIT_REVERSE 1

/** @def WINDOW_WIDTH
*	@brief Ancho de la ventana utilizada en la TTF
*/
#define WINDOW_WIDTH 1024

/** @def SAMPLE_FREC_US
*	@brief Ancho de la ventana utilizada en la TTF
*/
#define SAMPLE_FREC_US (1000000/1024)

/*==================[internal functions declaration]=========================*/
/**
 * @brief Lee el valor convertido del AD
 *
 */
void ReadData();
 /**
  *  @brief Arranca la conversión AD
  *
  */
void StartConvertion();
/**
 *  @brief Se realizan las inicializaciones de los drivers utilizados
 *
 */
void HardConfig();
/**
 *  @brief Función que permite apagar el afinador o evitar que se envie el valor de
 * frecuencia de la cuerda por el puerto serie. Esto sucede cuando se presionan las
 * teclas correspondientes.
 */
void DoSerial();
/**
 *  @brief Función que reconoce el número de la cuerda que se tocó
 */
void ReconocerCuerda();
/**
 *  @brief Función que prende los LEDs de la EDU CIAA en función de la frecuencia
 * de la cuerda
 */
void PrenderLeds();
/**
 *  @brief Función que calcula la TTF
 */
void CalcularTransformada();
/**
 *  @brief Función que envía la frecuencia de la cuerda por el puerto serie
 */
void EnviarFrecuencia();

/*==================[internal data definition]===============================*/

const uint16_t frecuencias[]={
	330,245,196,147,110,82,
}; /**<Vector que contiene las frecuencias ideales de las cuerdas de una guitarra*/
bool apagar=false; /**<Booleano cuyo estado permite apagar el afinador*/
bool mostrar_frecuencia=true;/**<Booleano cuyo estado permite enviar la frecuencia por el puerto serie*/
bool new_window = false; /**<Booleano que indica cuando se completó la ventana utilizada en la TTF*/
float32_t signal[2*WINDOW_WIDTH]; /**<Vector donde coloco los valores digitales de la conversión AD*/
serial_config uart_config={SERIAL_PORT_PC, 115200, DoSerial}; /**<Estructura para configurar el puerto serie*/
analog_input_config AD_config={CH1, AINPUTS_SINGLE_READ,ReadData}; /**<Estructura para configurar el conversor*/
timer_config timer={TIMER_C,SAMPLE_FREC_US,StartConvertion};/**<Estructura para configurar el TIMER_C*/
float32_t fft[WINDOW_WIDTH]; /**<Vector donde guardo los valores de la TTF*/
float32_t max = 0; /**<Variable que contiene el valor máximo de la TTF*/
uint8_t cuerda; /**<Variable que indica el numero de cuerda que se tocó*/
uint16_t i_max;/**<Variable que indica el subíndice del máximo de la TTF -valor de la frecuencia de la cuerda-*/




/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
void ReadData(){
	static uint16_t i = 0;
	uint16_t buf;
	AnalogInputRead(CH1, &buf);
	if(i<WINDOW_WIDTH){
		signal[i*2] = buf*(3.3/1024); /* Parte real */
		signal[i*2+1] = 0; /* Parte imaginaria */
		i++;
	}
	else{
		i = 0;
		new_window = true;
	}
}

void StartConvertion () {
	AnalogStartConvertion();
}

void ReconocerCuerda (){


	for(int j=0;j<CANT_CUERDAS;j++)
	{
			if(((i_max>frecuencias[j]-TOLERANCIA) && (i_max<frecuencias[j])) || ((i_max<frecuencias[j]+TOLERANCIA) && (i_max>frecuencias[j])))
			{
				cuerda=j+1;
			}

	}
}

void PrenderLeds(){

	LedsOffAll();

	if(i_max>frecuencias[cuerda-1]+LIMITE_2 && i_max<=frecuencias[cuerda-1] + LIMITE_3){
		LedOn(LED_3);
		EnviarFrecuencia();
	}


	else if(i_max>frecuencias[cuerda-1]+LIMITE_1 && i_max<=frecuencias[cuerda-1] + LIMITE_2 ){
		LedOn(LED_2);
		EnviarFrecuencia();
	}


	else if(i_max>=frecuencias[cuerda-1]-LIMITE_1 && i_max <=frecuencias[cuerda-1]+LIMITE_1){
		LedOn(LED_3);
		LedOn(LED_2);
		LedOn(LED_1);
		LedOn(LED_RGB_B);
		EnviarFrecuencia();
	}

	else if(i_max>=frecuencias[cuerda-1]-LIMITE_2 && i_max<frecuencias[cuerda-1]-LIMITE_1)
	{
		LedOn(LED_1);
		EnviarFrecuencia();
	}

	else if(i_max>=frecuencias[cuerda-1]-LIMITE_3 && i_max<frecuencias[cuerda-1]-LIMITE_2){
		LedOn(LED_RGB_B);
		EnviarFrecuencia();
	}

	else
		LedsOffAll();

}

void CalcularTransformada(){

	uint16_t i;
	/* Procesamiento para realizar la FFT */
	arm_cfft_f32(&arm_cfft_sR_f32_len1024, signal, IFFT_FLAG, DO_BIT_REVERSE);
	arm_cmplx_mag_f32(signal, fft, WINDOW_WIDTH);
	new_window = false;

	/* Obtencion del valor maximo del espectro de frecuencias */
	for(i=2 ; i<WINDOW_WIDTH ; i++)
	{
		if(fft[i]>max)
		{
			max = fft[i];
			i_max = i;
		}
	}
	max = 0;

}

void EnviarFrecuencia(){

	if(mostrar_frecuencia)
	{
	UartSendString(SERIAL_PORT_PC, "Frecuencia: ");
	UartSendString(SERIAL_PORT_PC, UartItoa(i_max,10));
	UartSendString(SERIAL_PORT_PC, "\r\n");
	}

}

void DoSerial(){

	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);

	switch(dato)
	{
	case 'A':
		apagar=!apagar;
		break;
	case 'S':
		mostrar_frecuencia=!mostrar_frecuencia;
		break;
	}


}

void HardConfig(){

	UartInit(&uart_config);
	SystemClockInit();
	fpuInit();
	LedsInit();
	AnalogInputInit(&AD_config);
	TimerInit(&timer);
	TimerStart(TIMER_C);

}

int main(void)
{

	HardConfig();


    while(1)
    {
    	if(!apagar)
    		if(new_window)
    		{
    			CalcularTransformada();
    			ReconocerCuerda();
    			PrenderLeds();
    		}
    }


    
	return 0;
}

/*==================[end of file]============================================*/

