/* Copyright 2020,
 * Santiago Beron
 * santiib98@outlook.com.ar
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * SB			Santiago Beron
 */

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 *Este programa  realiza el sensado, a través de un micrófono, de los sonidos provenientes
 *de las cuerdas de la guitarra. Realiza la conversión AD y le calcula a la señal resultante la TTF.
 *Una vez hecho esto, se encuentra el subíndice del valor máximo, que corresponde a la frecuencia con
 *la que sonó la cuerda. En función de esta frecuencia, se reconoce el número de cuerda y se prenden
 *los LEDS de la EDU-CIAA según la distancia de esta frecuencia respecto a la frecuencia de la
 *cuerda afinada. Cuando estas coinciden se prenden todos los LEDS.
 *
 * <a href="https://www.youtube.com/watch?v=esdqPHDWtcU&feature=youtu.be">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |    Ky-038 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	   +		| 	   +5V		|
 * | 	   G   	 	| 	   GND     	|
 * | 	  AO    	| 	   CH1     	|

 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                                    |
 * |:----------:|:------------------------------------------------|
 * | 30/10/2020 | Reconocimiento de cuerdas 					  |
 * | 31/10/2020	| Prender LEDs en función de la frecuencia 	      |
 * | 03/11/2020	| Enviar frecuencia por puerto serie	          |
 * | 04/10/2020	| Opcion de manejar programa al presionar teclas  |
 * | 05/10/2020	| Se documentó el código									  |
 *
 *

 *
 * @author Santiago Beron
 *
 */

#ifndef _AFINADOR_GUITARRA_H
#define _AFINADOR_GUITARRA_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _AFINADOR_GUITARRA_H */

