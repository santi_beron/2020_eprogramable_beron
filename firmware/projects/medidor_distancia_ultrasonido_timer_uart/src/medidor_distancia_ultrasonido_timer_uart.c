/** \addtogroup Proyectos
 ** @{ */
/** \addtogroup Medidor distancia ultrasonido timer uart
 ** @{ */

/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Santiago Beron
 *
 * Revisión:
 * 14-08-20: Versión inicial
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Medidor de distancia con sensor ultrasonido, interrupciones, timer y puerto serie
*
* @section genDesc Descripción general
*
* Este programa se ha escrito con el objetivo de probar el funcionamiento del driver del
* sensor ultrasonido y se ha modificado agregando interrupciones, funciones del timer y del puerto serie
*/


/*==================[inclusions]=============================================*/
#include "medidor_distancia_ultrasonido_timer_uart.h"
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/** @def DISTANCIA_0
*	@brief distancia correspondiente a 0 cm
*/
#define DISTANCIA_0 0
/** @def DISTANCIA_1
*	@brief distancia correspondiente a 10 cm
*/
#define DISTANCIA_1 10
/** @def DISTANCIA_2
*	@brief distancia correspondiente a 20 cm
*/
#define DISTANCIA_2 20
/** @def DISTANCIA_3
*	@brief distancia correspondiente a 30 cm
*/
#define DISTANCIA_3 30

/*==================[internal data definition]=================================*/

uint8_t activo=0; /**< Variable cuyo estado indica si se realiza la medición*/
uint8_t hold=0; /**< Variable que indica si se mantiene el resultado de la medicion */
int16_t distancia; /**< Variable que indica la distancia a la cual se encuentra el objeto */


/*==================[internal functions declaration]=========================*/


/**
 * @brief Enciende los LEDS en funcion de la distancia
 */
void MostrarDistancia(void)
{
	if(distancia>DISTANCIA_0 && distancia<= DISTANCIA_1)
	{
		ApagarLeds();
		DelayMs(10);
		LedOn(LED_RGB_B);
	}

	if(distancia>DISTANCIA_1 && distancia<= DISTANCIA_2)
	{
		ApagarLeds();
		DelayMs(10);
		LedOn(LED_RGB_B);
		LedOn(LED_1);
	}

	if(distancia>DISTANCIA_2 && distancia<= DISTANCIA_3)
	{
		ApagarLeds();
		DelayMs(10);
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
	}

	if(distancia>DISTANCIA_3)
	{
		ApagarLeds();
		DelayMs(10);
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
	}

}

/**
 * @brief Apaga los LEDs
 */
void ApagarLeds(void)
{
	LedOff(LED_3);
	LedOff(LED_2);
	LedOff(LED_1);
	LedOff(LED_RGB_B);

}
/**
 * @brief Detecta si la tecla 1 fue presionada y cambia el estado de la variable activo
 */
void ISR_Tecla1(void)
{
	activo=!activo;
}

/**
 * @brief Detecta si la tecla 2 fue presionada y cambia el estado de la variable hold
 */
void ISR_Tecla2(void)
{
	hold=!hold;
}


/**
 * @brief Cambio los estados de las variables hold y activo en caso de que se presionen las teclas correspondientes del teclado
 */
void DoSerial(void){
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);

	switch(dato)
	{
	case 'H':
		activo=!activo;
		break;
	case 'O':
		hold=!hold;
		break;
	}

}
/**
 * @brief Envio la información de la distancia medida por medio del puerto serie
 */
void EnviarInformacion()
{
	UartSendString(SERIAL_PORT_PC, UartItoa(distancia,10));
	UartSendString(SERIAL_PORT_PC, " cm\r\n");
	UartSendString(SERIAL_PORT_PC, " ");
}

/**
 * @brief Actualizo la medición
 */
void ActualizarMuestra(void)
{

	if(activo==FALSE)
			ApagarLeds();

	if(activo==TRUE)
		{

		HcSr04GetTimeUs(GPIO_T_FIL2,GPIO_T_FIL3);
		EnviarInformacion();


			if(hold==FALSE)
			{
				distancia=HcSr04ReadDistanceCentimeters();
				MostrarDistancia();

			}
		}
}



/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Configuro el timer y el puerto serie
	timer_config  timer_init ={TIMER_A,1000, ActualizarMuestra};
	serial_config miPuerto={SERIAL_PORT_PC, 9600, DoSerial};

	//Funciones de inicialización
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, ISR_Tecla1);
	SwitchActivInt(SWITCH_2, ISR_Tecla2);
	HcSr04Init(GPIO_T_FIL2,	GPIO_T_FIL3);
	TimerInit(&timer_init);
	UartInit(&miPuerto);

	TimerStart(TIMER_A);


	while(1)
	{

	}
	return 0;

}

/*==================[end of file]============================================*/

