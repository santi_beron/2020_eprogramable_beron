/* Copyright 2020,
 * Santiago Beron
 * santiib98@outlook.com.ar
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * SB			Santiago Beron
 */

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * Este programa prende los LEDs de la placa EDU - CIAA según la distancia a la cual se encuentre
 * un objeto del sensor ultrasonido. Si el objeto está entre 0 y 10 cm se enciende el LED_RGB_B,
 * si está entre 10 y 20 cm, se enciende el LED_RGB_B y LED_1, si la distancia está en el rango entre
 * 20 y 30 cm se enciende el LED_RGB_B, LED_1 y LED_2. Si la distancia es mayor a 30 cm, se enciende los
 * los LEDs del caso anterior y además el LED_3.
 *
 * La medición se lleva a cabo cuando la TEC1 se presiona y se desactiva cuando se vuelve a presionar. La TEC2
 * se utiliza para mantener el resultado, si se vuelve a presionar, se vuelve a medir. En esta versión, esto se ha
 * implementado con interrupciones.
 *
 * Se ha implementado el programa con el timer, de tal manera que la actualización de la medición se lleva a cabo
 * con este driver. Además, se han incorporado funcionalidades relacionadas con el puerto serie, de tal manera que ahora
 * se muestra la medición por pantalla y se puede cambiar también el estado de las variables hold y activo mediante el teclado.
 *
 * <a href="https://www.youtube.com/watch?v=c5WUvX0N0Gk&feature=youtu.be">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Sensor Ultrasonido		|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	T_FIL2	             	| 	 ECHO		|
 * | 	T_FIL3	 	            | 	 TRIGGER 	|
 * | 	+5V	 	                | 	 +5V		|
 * | 	GND 	 	            |    GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                             |
 * |:----------:|:--------------------------------------------------------|
 * | 12/09/2020 | Creación del documento e implementación del programa    |
 * | 19/09/2020	| Se implementa el programa con interrupciones            |
 * | 24/09/2020	| Se implememta el programa con el timer             	  |
 * | 01/10/2020	| Se implememta el programa con el puerto serie       	  |
 *
 *
 * @author Santiago Beron
 *
 */

#ifndef _MEDIDOR_DISTANCIA_ULTRASONIDO_UART_H
#define _MEDIDOR_DISTANCIA_ULTRASONIDO_UART_H



/*==================[inclusions]=============================================*/



/*==================[macros and definitions]=================================*/
int main(void);



/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

